# Neutron - A react-like Haskell Gtk Binding

Neutron aims to bring the ease of frontend development to Gtk. It works similarly to react, in that the state is used to create a view, with the framework doing the heavy lifting of keeping the view updated.

# Example
This is the bundled ToDo example:

``` haskell

main :: IO ()
main = widgetMain (ToDoState "" mempty) $ do
  ToDoState currentItem items <- ask
  window [#title := ("ToDo" :: T.Text)] $ do
    vBox [] $ do
      hBox [] $ do
        entry <- widget TextEntry [#placeholderText := ("Add an item" :: T.Text), #text := currentItem]
        addButton <- widget Button [#label := ("Add" :: T.Text)]
        onEvent entry textChanged $ \t -> do
          modify $ \s -> s { currentItem = t}
        onEvent addButton clicked . const . modify
          $ \(ToDoState current items) -> ToDoState { currentItem = "", items = current : items}
      vBox [] $ 
        mapM_ (\t -> widget Label [#label := t]) items

data ToDoState = ToDoState { currentItem :: T.Text, items :: [T.Text] }
  deriving (Show, Typeable)
  
```