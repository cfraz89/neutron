{ mkDerivation, haskellPackages, base, containers, exceptions, gi-gdk, gi-gio
, gi-glib, gi-gobject, gi-gtk, haskell-gi-base, hpack, lens
, monad-control, mtl, stdenv, stm, text, transformers
, uuid, gi-webkit2
}:
let streamly = haskellPackages.callHackage "streamly" "0.4.1" {};
in
mkDerivation {
  pname = "neutron";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base containers exceptions gi-gdk gi-gio gi-glib gi-gobject gi-gtk
    haskell-gi-base lens monad-control mtl stm streamly text
    transformers uuid gi-webkit2
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base containers exceptions gi-gdk gi-gio gi-glib gi-gobject gi-gtk
    haskell-gi-base lens monad-control mtl stm streamly text
    transformers uuid
  ];
  testHaskellDepends = [
    base containers exceptions gi-gdk gi-gio gi-glib gi-gobject gi-gtk
    haskell-gi-base lens monad-control mtl stm streamly text
    transformers uuid
  ];
  preConfigure = "hpack";
  license = stdenv.lib.licenses.bsd3;
}
