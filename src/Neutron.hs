module Neutron
( neutron
, module Neutron.Widget
, module Neutron.Event
, module Neutron.Frontend
, module Neutron.Props.WidgetProp
) where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.State.Lazy as State
import Data.GI.Base
import Data.GI.Gtk.Threading
import Data.Semigroup
import qualified GI.Gdk as Gdk
import qualified GI.Gio as Gio
import GI.GObject
import qualified GI.Gtk as Gtk
import Streamly
import Data.Typeable
import Neutron.Widget
import Neutron.Backend
import qualified Neutron.Diff as D
import Neutron.Event
import Neutron.WidgetId
import Neutron.WidgetTree
import Neutron.Frontend
import Neutron.Props.WidgetProp (WidgetPropData(..))

-- | Main entrypoint into Neutron
neutron :: Typeable s => Maybe String -> s -> (s -> WidgetT s (SerialT IO) a) -> IO ()
neutron cssFile s app = do
  Gtk.init Nothing
  setCurrentThreadAsGUIThread
  initialiseStyling =<< (traverse Gio.fileNewForPath cssFile)
  stateChan <- newTChanIO
  atomically $ writeTChan stateChan s
  widgetMapTVar <- newTVarIO mempty
  eventsSuppressedTVar <- newTVarIO False
  handlersTVar <- newTVarIO mempty
  windowTVar <- newTVarIO Nothing
  let wts = WidgetTreeState stateChan widgetMapTVar eventsSuppressedTVar handlersTVar windowTVar
  forkIO . runStream $ do
    renderLoop s app wts mempty
  Gtk.main
    where widgets (a, s, w) = w

-- | Main loop of a neutron app
-- state updates -> render -> state updates etc
renderLoop :: Typeable s => s -> (s -> WidgetT s (SerialT IO) a) -> WidgetTreeState s -> [Decl s] -> SerialT IO ()
renderLoop s app wts decls = do
  handlers <- liftIO . atomically $ do
    h <- readTVar . wtsEventHandlers $ wts
    writeTVar (wtsEventHandlers wts) mempty
    pure h
  mapM (\(object, id) -> signalHandlerDisconnect object id) handlers
  newDecls <- snd <$> runWidgetT (app s) Root
  let actions = D.diff decls newDecls
  runWidgetTree (buildWidgetTree @_ @Gtk.Window Nothing actions s) wts
  s' <- liftIO . atomically $ readTChan (wtsState wts)
  renderLoop s' app wts newDecls

initialiseStyling :: Gio.IsFile f => Maybe f -> IO ()
initialiseStyling f = Gdk.screenGetDefault >>= \case
  Nothing -> pure ()
  Just screen -> do
    cssProvider <- Gtk.cssProviderNew
    maybe mempty (Gtk.cssProviderLoadFromFile cssProvider) f
    Gtk.styleContextAddProviderForScreen screen cssProvider (fromIntegral Gtk.STYLE_PROVIDER_PRIORITY_USER)
