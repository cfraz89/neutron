module Neutron.Event where

import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Monad.IO.Class
import Data.GI.Base.Signals
import Data.Proxy
import Data.Semigroup
import qualified Data.Text as T
import qualified GI.Gtk as Gtk

import Streamly
import qualified Streamly.Prelude as S
import Neutron.Common
import Foreign.C.Types

voidEvent :: (StreamConstraint t m, Gtk.GObject object, SignalInfo info, MonadIO n, HaskellCallbackType info ~ n (), Semigroup (t m a), MonadIO m)
  => object
  -> Gtk.SignalProxy object info 
  -> (object -> n a)
  -> m (CULong, t m a)
voidEvent obj ev f = do
    chan <- liftIO newTChanIO
    handlerId <- on obj ev $ f obj >>= (liftIO . atomically . writeTChan chan)
    pure (handlerId, cycle1 . S.yieldM . liftIO . atomically . readTChan $ chan)

data EventType 
  = TextChanged
  | Clicked
  | SearchChanged

class HasEvent w (e :: EventType) a | w e -> a where
  widgetEvent :: Proxy e -> w -> IO (CULong, SerialT IO a)

searchChanged :: Proxy 'TextChanged
searchChanged = Proxy

textChanged :: Proxy 'TextChanged
textChanged = Proxy

clicked :: Proxy 'Clicked
clicked = Proxy
