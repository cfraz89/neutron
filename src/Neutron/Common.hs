module Neutron.Common where

import Control.Monad.Catch
import Control.Monad.IO.Class
import Control.Monad.Trans.Control
import Data.Functor
import Data.GI.Gtk.Threading
import Data.Semigroup
import qualified GI.Gdk as Gdk
import Streamly

type StreamConstraint t m = (IsStream t, MonadIO m, Monad (t m), MonadIO (t m), Semigroup (t m ()), MonadBaseControl IO m, MonadThrow m)

data Demoted = Demoted

demote :: Functor f => f w -> f Demoted
demote wp = const Demoted <$> wp
