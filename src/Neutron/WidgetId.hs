module Neutron.WidgetId where

import Data.String

data WidgetIdParent = WidgetIdParent { widpParent :: WidgetIdParent, widpIndex :: Int } | Root
  deriving (Show, Eq, Ord)

data WidgetId = WidgetId { widParent :: WidgetIdParent, widIndex :: Int }
  deriving (Show, Eq, Ord)