module Neutron.WidgetTree where

import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Monad.Reader
import Control.Monad.Reader.Class
import Control.Monad.Trans
import qualified Data.Map as M
import Foreign.C.Types
import GI.GObject
import qualified GI.Gtk as Gtk

import Neutron.Widget
import Neutron.WidgetId
import Neutron.Common

-- The instantiation of the ast
newtype WidgetTree s m a = WidgetTree { unWidgetTree :: ReaderT (WidgetTreeState s) m a }
  deriving (MonadIO, Monad, Functor, Applicative, MonadReader (WidgetTreeState s)) 

instance MonadTrans (WidgetTree s) where
  lift = WidgetTree . lift

instance (Semigroup (t m a), StreamConstraint t m) => Semigroup (WidgetTree s (t m) a) where
  (WidgetTree r1) <> (WidgetTree r2) = WidgetTree $ do
    st <- ask
    let s1 = runReaderT r1 st
        s2 = runReaderT r2 st
    lift $ s1 <> s2

runWidgetTree :: Monad m => WidgetTree s m a -> WidgetTreeState s -> m a
runWidgetTree wt wtState = flip runReaderT wtState . unWidgetTree $ wt

--Everything the backend needs to run
data WidgetTreeState s = WidgetTreeState
  { wtsState :: TChan s
  , wtsWidgetMap :: TVar WidgetMap
  , wtsEventsSupressed :: TVar Bool
  , wtsEventHandlers :: TVar [(Object, CULong)]
  , wtsWindow :: TVar (Maybe Gtk.Window)
  }
