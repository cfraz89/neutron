module Neutron.Widget where

import Control.Lens
import Control.Monad.Reader
import Control.Monad.Reader.Class
import Control.Monad.State.Class
import Control.Monad.State.Lazy
import Control.Monad.Writer.Class
import Control.Monad.Writer.Lazy
import Control.Monad.Trans
import qualified Data.Map as M
import Data.Proxy
import qualified Data.Text as T
import GI.GObject
import qualified GI.Gtk as Gtk
import Streamly

import Neutron.Common
import Neutron.Event
import Neutron.WidgetId
import Neutron.Props.Internal
import Neutron.Props.WidgetProp
import Data.Typeable
import Data.GI.Base.Attributes
import qualified GI.WebKit2.Objects.WebView as WebView

data Widget s w = Widget { widgetId :: WidgetId, widgetConstructor :: WidgetTag s w, widgetProps :: [WidgetProp w] }
  deriving (Typeable)

data WidgetTag s w where
  Window :: [Decl s] -> WidgetTag s Gtk.Window
  Button :: WidgetTag s Gtk.Button
  LinkButton :: WidgetTag s Gtk.LinkButton
  Entry :: WidgetTag s Gtk.Entry
  SearchEntry :: WidgetTag s Gtk.SearchEntry
  Label :: WidgetTag s Gtk.Label
  Menu :: WidgetTag s Gtk.Menu
  WebView :: WidgetTag s WebView.WebView
  HBox :: [Decl s] -> WidgetTag s Gtk.HBox
  VBox :: [Decl s] -> WidgetTag s Gtk.VBox
  HeaderBar :: [Decl s] -> WidgetTag s Gtk.HeaderBar
  SearchBar :: [Decl s] -> WidgetTag s Gtk.SearchBar
    deriving Typeable

data DWidget s = forall w. (Typeable w, Gtk.IsWidget w, NeutronWidget w) => DWidget {undWidget :: (Widget s w) }
    deriving Typeable
data DWidgetTag s = forall w. (Typeable w, Gtk.IsWidget w, NeutronWidget w) => DWidgetTag { undWidgetTag :: (WidgetTag s w)}
    deriving Typeable

-- Decls that rely on state
data Decl s where
  DeclWidget :: DWidget s -> Decl s 
  DeclStateEffect :: DStateEffect s -> Decl s

data DStateEffect s where
  DStateEffect :: (HasEvent w e a, IsObject w, NeutronWidget w) => Proxy (e :: EventType) -> Widget s w -> (a -> StateEffect s ()) -> DStateEffect s

-- The monad that we build the AST in
-- Writes declarations that will build the final tree
-- States the relevant Parent
newtype WidgetT s m a = WidgetT { unWidgetT :: StateT (WidgetIdParent, Int) (WriterT [Decl s] m) a }
  deriving (Functor, Applicative, Monad, MonadState (WidgetIdParent, Int), MonadWriter [Decl s], MonadIO)

instance MonadTrans (WidgetT s) where
  lift = WidgetT . lift . lift

runWidgetT :: Functor m => WidgetT s m a -> WidgetIdParent -> m (a, [Decl s])
runWidgetT wt widp = pick <$> runWriterT (runStateT (unWidgetT wt) (widp, 0))
  where pick ((a, _), d) = (a, d)

--Monad for an effect that has access to all our widgets and can modify our state
newtype StateEffectT s m a = StateEffectT { unStateEffectT :: ReaderT WidgetMap (StateT s (SerialT m)) a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadReader WidgetMap, MonadState s)

instance MonadTrans (StateEffectT s) where
  lift = StateEffectT . lift . lift . lift

type StateEffect s a = StateEffectT s IO a

type WidgetMap = M.Map WidgetId Gtk.Widget

data Event w s (e :: EventType) a = Event { eventWidget :: Widget s w }

fromWidget :: (MonadIO m, NeutronWidget w) => p w -> Gtk.Widget -> m w
fromWidget _ = liftIO . unsafeCastTo wrapper


-- instance WidgetWrapper WebView.WebView where
class GObject w => NeutronWidget w where
  type ConstructData w :: *
  wrapper :: ManagedPtr w -> w
  construct :: MonadIO m => ConstructData w -> m w
  setProp :: MonadIO m => w -> WidgetProp Demoted -> m ()
