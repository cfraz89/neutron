module Neutron.StreamOps where

import Control.Monad.Reader
import Streamly

import Neutron.Common
import Neutron.WidgetTree

doParallel :: StreamConstraint t m => WidgetTree s (t m) () -> WidgetTree s (t m) () -> WidgetTree s (t m) ()
doParallel (WidgetTree r1) (WidgetTree r2) = WidgetTree $ do
  st <- ask
  let s1 = runReaderT r1 st
      s2 = runReaderT r2 st
  lift $ s1 `parallel` s2

doSerial :: StreamConstraint t m => WidgetTree s (t m) () -> WidgetTree s (t m) () -> WidgetTree s (t m) ()
doSerial (WidgetTree r1) (WidgetTree r2) = WidgetTree $ do
  st <- ask
  let s1 = runReaderT r1 st
      s2 = runReaderT r2 st
  lift $ s1 `serial` s2

doWSequential :: StreamConstraint t m => WidgetTree s (t m) () -> WidgetTree s (t m) () -> WidgetTree s (t m) ()
doWSequential (WidgetTree r1) (WidgetTree r2) = WidgetTree $ do
  st <- ask
  let s1 = runReaderT r1 st
      s2 = runReaderT r2 st
  lift $ s1 `wSerial` s2

doAsync :: StreamConstraint t m => WidgetTree s (t m) () -> WidgetTree s (t m) () -> WidgetTree s (t m) ()
doAsync (WidgetTree r1) (WidgetTree r2) = WidgetTree $ do
  st <- ask
  let s1 = runReaderT r1 st
      s2 = runReaderT r2 st
  lift $ s1 `async` s2

wtSerially :: StreamConstraint t m => WidgetTree s (t m) () -> WidgetTree s (SerialT m) ()
wtSerially (WidgetTree r1) = WidgetTree $ do
  st <- ask
  let s1 = runReaderT r1 st
  lift $ serially . adapt $ s1

wtParallel :: StreamConstraint t m => WidgetTree s (t m) () -> WidgetTree s (ParallelT m) ()
wtParallel (WidgetTree r1) = WidgetTree $ do
  st <- ask
  let s1 = runReaderT r1 st
  lift $ parallely . adapt $ s1