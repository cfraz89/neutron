module Neutron.Widget.Base where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Control.Monad.IO.Class
import Data.GI.Base.Attributes
import Neutron.Common
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))

instance Gtk.IsWidget w => HasProp "visible" Bool w where
  prop = Visible

instance Gtk.IsWidget w => HasProp "vexpand" Bool w where
  prop = VExpand

instance Gtk.IsWidget w => HasProp "hexpand" Bool w where
  prop = HExpand

instance Gtk.IsWidget w => HasProp "name" T.Text w where
  prop = Name

setPropWidget :: MonadIO m => Gtk.IsWidget w => w -> WidgetProp Demoted -> m ()
setPropWidget w = \case
  Visible v -> Gtk.setWidgetVisible w v
  VExpand v -> Gtk.widgetSetVexpand w v >> Gtk.setWidgetValign w Gtk.AlignStart
  HExpand h -> Gtk.widgetSetHexpand w h
  Name n -> Gtk.widgetSetName w n