module Neutron.Widget.SearchBar where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import qualified Neutron.Props.WidgetProp as WP
import Neutron.Widget.Base
import Neutron.Widget

instance HasProp "searchModeEnabled" Bool Gtk.SearchBar where
  prop = WP.SearchModeEnabled

instance NeutronWidget Gtk.SearchBar where
  type ConstructData Gtk.SearchBar = ()

  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.SearchBar

  setProp w (WP.SearchModeEnabled e) = set w [#searchModeEnabled := e]
  setProp w p = setPropWidget w p
  