module Neutron.Widget.Window where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget

instance HasProp "visible" Bool Gtk.Window where
  prop = Visible

instance HasProp "title" T.Text Gtk.Window where
  prop = Title

instance NeutronWidget Gtk.Window where
  type ConstructData Gtk.Window = ()

  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.Window

  setProp w (Title t) = set w [#title := t]
  setProp w p = setPropWidget w p

