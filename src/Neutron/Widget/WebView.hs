module Neutron.Widget.WebView where

import qualified GI.Gtk as Gtk
import GI.WebKit2.Objects.WebView as WebView
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget

instance HasProp "uri" T.Text WebView where
  prop = Uri

instance NeutronWidget WebView where
  type ConstructData WebView.WebView = ()

  construct _ = Gtk.new wrapper mempty

  wrapper = WebView.WebView
  
  setProp w (Uri t) = webViewLoadUri w t
  setProp w p = setPropWidget w p