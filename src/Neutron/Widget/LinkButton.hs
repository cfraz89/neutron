module Neutron.Widget.LinkButton where

import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp as WP (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget
import Neutron.Event

instance HasProp "label" T.Text Gtk.LinkButton where
  prop = WP.Label

instance HasProp "uri" T.Text Gtk.LinkButton where
  prop = WP.Uri

instance HasEvent Gtk.LinkButton 'Clicked () where
  widgetEvent _ b = voidEvent b #clicked (const (pure ()))

instance NeutronWidget Gtk.LinkButton where
  type ConstructData Gtk.LinkButton = ()

  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.LinkButton

  setProp w (WP.Label t) = set w [#label := t]
  setProp w (WP.Uri t) = set w [#uri := t]
  setProp w p = setPropWidget w p