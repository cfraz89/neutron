module Neutron.Widget.HeaderBar where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget

instance HasProp "title" T.Text Gtk.HeaderBar where
  prop = Title

instance HasProp "subtitle" T.Text Gtk.HeaderBar where
  prop = Subtitle

instance HasProp "showCloseButton" Bool Gtk.HeaderBar where
  prop = ShowCloseButton

instance NeutronWidget Gtk.HeaderBar where
  type ConstructData Gtk.HeaderBar = ()
  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.HeaderBar

  setProp w (Title p) = set w [#title := p]
  setProp w (Subtitle t) = set w [#subtitle := t]
  setProp w (ShowCloseButton s) = set w [#showCloseButton := s]
  setProp w p = setPropWidget w p