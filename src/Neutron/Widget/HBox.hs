module Neutron.Widget.HBox where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget

instance NeutronWidget Gtk.HBox where
  type ConstructData Gtk.HBox = ()
  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.HBox

  setProp w p = setPropWidget w p