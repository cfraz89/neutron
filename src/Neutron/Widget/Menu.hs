module Neutron.Widget.Menu where

import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp as WP (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget
import Neutron.Event

instance NeutronWidget Gtk.Menu where
  type ConstructData Gtk.Menu = ()

  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.Menu

  setProp w p = setPropWidget w p