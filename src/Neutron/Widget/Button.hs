module Neutron.Widget.Button where

import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp as WP (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget
import Neutron.Event

instance HasProp "label" T.Text Gtk.Button where
  prop = WP.Label

instance HasEvent Gtk.Button 'Clicked () where
  widgetEvent _ b = voidEvent b #clicked (const (pure ()))

instance NeutronWidget Gtk.Button where
  type ConstructData Gtk.Button = ()
  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.Button

  setProp w (WP.Label t) = set w [#label := t]
  setProp w p = setPropWidget w p