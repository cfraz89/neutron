module Neutron.Widget.VBox where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget

instance NeutronWidget Gtk.VBox where
  type ConstructData Gtk.VBox = ()

  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.VBox

  setProp w p = setPropWidget w p