module Neutron.Widget.Label where

import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import qualified Neutron.Props.WidgetProp as WP
import Neutron.Widget.Base
import Neutron.Widget

instance HasProp "label" T.Text Gtk.Label where
  prop = WP.Label

instance NeutronWidget Gtk.Label where
  type ConstructData Gtk.Label = ()
  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.Label

  setProp w (WP.Label t) = set w [#label := t]
  setProp w p = setPropWidget w p
  