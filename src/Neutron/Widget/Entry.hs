module Neutron.Widget.Entry where

import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import qualified Data.Text as T

import Data.GI.Base.Attributes
import Neutron.Props.Internal
import Neutron.Props.WidgetProp (WidgetProp(..))
import Neutron.Widget.Base
import Neutron.Widget
import Neutron.Event

instance HasProp "placeholderText" T.Text Gtk.Entry where
  prop = PlaceholderText

instance HasProp "text" T.Text Gtk.Entry where
  prop = Text

instance HasEvent Gtk.Entry 'TextChanged T.Text where
  widgetEvent _ entry = voidEvent entry #changed (`Gdk.get` #text)

instance NeutronWidget Gtk.Entry where
  type ConstructData Gtk.Entry = ()
  construct _ = Gtk.new wrapper mempty

  wrapper = Gtk.Entry

  setProp w (PlaceholderText p) = set w [#placeholderText := p]
  setProp w (Text t) = set w [#text := t]
  setProp w p = setPropWidget w p