module Neutron.Frontend where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.State.Lazy
import Control.Monad.Writer.Lazy
import qualified Data.Map as M
import Data.Proxy
import qualified Data.Text as T
import Data.UUID.V4
import qualified GI.GObject as GObject
import qualified GI.Gtk as Gtk
import Streamly
import Data.Typeable

import Neutron.Widget
import Neutron.Event
import Neutron.WidgetId
import Neutron.Widgets
import Neutron.Common
import Neutron.Props.Internal
import Neutron.Props.WidgetProp

-- Pluck the gtk widget instance out of our stateEffect
gtkWidget :: (MonadIO m, NeutronWidget w) => Widget s w -> StateEffectT s m w
gtkWidget widget@(Widget wid _ _) = do
  widgetMap <- ask
  fromWidget widget $ widgetMap M.! wid

container :: ( IsStream t
             , MonadAsync m
             , MonadIO (t m)
             , Typeable w
             , Gtk.IsWidget w
             , NeutronWidget w
             ) => ([Decl s] -> WidgetTag s w) -> [WidgetPropData w] -> WidgetT s (t m) a -> WidgetT s (t m) (Widget s w, a)
container cons props child = do
  (parent, idx) <- get
  (a, childDecls) <- lift $ runWidgetT child (WidgetIdParent parent idx)
  put (parent, succ idx)
  w <- widget (cons childDecls) props
  pure (w, a)

window :: (IsStream t, MonadAsync m, MonadIO (t m)) => [WidgetPropData Gtk.Window] -> WidgetT s (t m) a -> WidgetT s (t m) (Widget s Gtk.Window, a)
window = container Window

hBox :: (IsStream t, MonadAsync m, MonadIO (t m)) => [WidgetPropData Gtk.HBox] -> WidgetT s (t m) a -> WidgetT s (t m) (Widget s Gtk.HBox, a)
hBox = container HBox

vBox :: (IsStream t, MonadAsync m, MonadIO (t m)) => [WidgetPropData Gtk.VBox] -> WidgetT s (t m) a -> WidgetT s (t m) (Widget s Gtk.VBox, a)
vBox = container VBox

headerBar :: (IsStream t, MonadAsync m, MonadIO (t m)) 
  => [WidgetPropData Gtk.HeaderBar] -> WidgetT s (t m) a -> WidgetT s (t m) (Widget s Gtk.HeaderBar, a)
headerBar = container HeaderBar

searchBar :: (IsStream t, MonadAsync m, MonadIO (t m))
  => [WidgetPropData Gtk.SearchBar] -> WidgetT s (t m) a -> WidgetT s (t m) (Widget s Gtk.SearchBar, a)
searchBar = container SearchBar 

widget :: (Monad m, Typeable w, Gtk.IsWidget w, NeutronWidget w) => WidgetTag s w -> [WidgetPropData w] -> WidgetT s m (Widget s w)
widget ctr props = do
  (parent, idx) <- get
  let id = WidgetId parent idx
  let w = Widget id ctr (applyPropData <$> props)
  put (parent, succ idx)
  tell [DeclWidget (DWidget w)]
  pure w

stateEffect :: (HasEvent w e a, GObject.IsObject w, NeutronWidget w) => Event w s e a -> Proxy e -> (a -> StateEffect s ()) -> WidgetT s (SerialT IO) ()
stateEffect (Event widget) eProxy effect = do
  tell [DeclStateEffect $ DStateEffect eProxy widget effect]

onEvent :: (HasEvent w e a, GObject.IsObject w, NeutronWidget w) => Widget s w -> Proxy e -> (a -> StateEffect s ()) -> WidgetT s (SerialT IO) ()
onEvent widget eProxy effect = stateEffect (event eProxy widget) eProxy effect

event :: HasEvent w e a => Proxy e -> Widget s w -> Event w s e a
event _ widget = Event widget

noWidgets :: Monad m => WidgetT s m ()
noWidgets = pure ()