module Neutron.Diff where

import Data.List
import qualified Data.Text as T
import Data.Proxy
import qualified GI.Gtk as Gtk

import Neutron.Common
import Neutron.Widget
import Neutron.WidgetId
import Neutron.Widgets as W
import Data.Maybe
import Data.Typeable
import Neutron.Props.Internal
import Neutron.Props.WidgetProp
import Neutron.Widget as W

import Debug.Trace

data Action s = UpdateProps WidgetId (DWidgetTag s) [WidgetProp Demoted]
              | UpdateChildren (DWidget s) ContainerType [Action s]
              | DeleteWidget (DWidget s)
              | AddWidget (DWidget s)
              | ActionStateEffect (DStateEffect s)

instance Show (Action s) where
  show (UpdateProps _ _ _) = "Update props"
  show (UpdateChildren _ _ _) = "Update children"
  show (DeleteWidget _) = "Delete Widget"
  show (AddWidget _) = "AddWidget"
  show (ActionStateEffect _) = "State Effect"

data ContainerType
  = CtrWindow
  | CtrHBox
  | CtrVBox
  | CtrHeaderBar
  | CtrSearchBar

withContainer :: ContainerType -> (forall c. (Gtk.IsContainer c, NeutronWidget c) => Proxy c -> b) -> b
withContainer ct f = case ct of
  CtrWindow -> f $ Proxy @Gtk.Window
  CtrHBox -> f $ Proxy @Gtk.HBox
  CtrVBox -> f $ Proxy @Gtk.VBox
  CtrHeaderBar -> f $ Proxy @Gtk.HeaderBar
  CtrSearchBar -> f $ Proxy @Gtk.SearchBar

diff :: Typeable s => [Decl s] -> [Decl s] -> [Action s]
diff decls1 decls2 = concatMap actionOn (zip decls1 decls2)
                     <> diffRemainder decls1 decls2

actionOn :: Typeable s => (Decl s, Decl s) -> [Action s]
actionOn = \case
          (DeclWidget dw1@(DWidget w1), DeclWidget dw2@(DWidget w2)) -> 
            case cast w2 of
              Nothing -> [DeleteWidget dw1, AddWidget dw2]
              Just castedW2 -> let diffedProps = diffProps w1 castedW2 in
                case (widgetConstructor w1, widgetConstructor w2) of
                  (Window d1, Window d2) -> diffedProps <> [UpdateChildren dw2 CtrWindow $ diff d1 d2]
                  (HBox h1, HBox h2) -> diffedProps <> [UpdateChildren dw2 CtrHBox $ diff h1 h2]
                  (VBox v1, VBox v2) -> diffedProps <> [UpdateChildren dw2 CtrVBox $ diff v1 v2]
                  (HeaderBar hb1, HeaderBar hb2) -> diffedProps <> [UpdateChildren dw2 CtrHeaderBar $ diff hb1 hb2]
                  (Button, Button) -> diffedProps
                  (LinkButton, LinkButton) -> diffedProps
                  (Entry, Entry) -> diffedProps
                  (SearchEntry, SearchEntry) -> diffedProps
                  (W.Label, W.Label) -> diffedProps
                  (WebView, WebView) -> diffedProps
                  (SearchBar sb1, SearchBar sb2) -> diffedProps <> [UpdateChildren dw2 CtrSearchBar $ diff sb1 sb2]
                  (Menu, Menu) -> diffedProps
                  _ -> [DeleteWidget dw1, AddWidget dw2]
          (DeclWidget w, DeclStateEffect se) -> [DeleteWidget w, ActionStateEffect se]
          (DeclStateEffect se, DeclWidget w) -> pure $ AddWidget w
          (DeclStateEffect se1, DeclStateEffect se2) -> pure $ ActionStateEffect se2

diffProps :: (Typeable w, Gtk.IsWidget w, NeutronWidget w) => Widget s w -> Widget s w -> [Action s]
diffProps (Widget _ _ w1props) w2@(Widget w2id w2cons w2props) = 
  case mapMaybe (checkProp w1props) w2props of
    [] -> mempty
    diffedProps -> pure $ UpdateProps w2id (DWidgetTag w2cons) diffedProps

checkProp :: [WidgetProp w] -> WidgetProp w -> Maybe (WidgetProp Demoted)
checkProp lastProps prop = case find (== prop) lastProps of
  Nothing -> Just (demote prop)
  Just _ -> Nothing
                                                               
                                
diffRemainder :: [Decl s] -> [Decl s] -> [Action s]
diffRemainder decls1 decls2 | length decls1 == length decls2 = mempty
                            | length decls1 < length decls2 = concatMap addDecl $ drop (length decls1) decls2
                            | length decls1 > length decls2 = concatMap deleteDecl $ drop (length decls2) decls1
  where addDecl = \case
          DeclWidget w -> pure $ AddWidget w
          DeclStateEffect se -> pure $ ActionStateEffect se

        deleteDecl = \case
          DeclWidget w -> pure $ DeleteWidget w
          _ -> mempty
