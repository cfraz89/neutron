{-# LANGUAGE AllowAmbiguousTypes #-}

module Neutron.Props.Internal where

import qualified GI.Gtk as Gtk
import Data.Proxy

import GHC.TypeLits (Symbol)
import GHC.OverloadedLabels as OL
import qualified Data.GI.Base.Attributes as A
import Control.Monad.IO.Class

import Neutron.Common
import Neutron.Props.WidgetProp

applyPropData :: WidgetPropData w -> WidgetProp w
applyPropData (f := v) = f v
applyPropData (f :~ t) = f t

class HasProp (k :: Symbol) v w  | k -> v where
  prop :: v -> WidgetProp w

instance HasProp p v w => OL.IsLabel (p :: Symbol) (v -> WidgetProp w) where
  fromLabel = prop @p 