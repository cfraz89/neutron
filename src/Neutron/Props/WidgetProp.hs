module Neutron.Props.WidgetProp where

import qualified Data.Text as T

data WidgetPropData w where
  (:=) :: (v -> WidgetProp w) -> v -> WidgetPropData w
  (:~) :: (T.Text -> WidgetProp w) -> T.Text -> WidgetPropData w

-- We use the type constraint to restrict which properties can be set on which widgets
-- The type param isn't actually used, but its needed for the type inference for labels
-- to be able to construct this type
data WidgetProp w = Visible Bool 
                  | Title T.Text
                  | Subtitle T.Text
                  | PlaceholderText T.Text
                  | Text T.Text
                  | Label T.Text
                  | ShowCloseButton Bool
                  | VExpand Bool
                  | HExpand Bool
                  | Name T.Text
                  | Uri T.Text
                  | SearchModeEnabled Bool
  deriving (Show, Eq, Functor)