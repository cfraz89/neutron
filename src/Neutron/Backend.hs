module Neutron.Backend where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Monad.IO.Class
import Control.Monad.Reader
import Control.Monad.State.Lazy
import Control.Monad.Writer.Lazy
import Data.GI.Base
import Data.GI.Base.Constructible
import Data.GI.Base.Attributes 
import Data.GI.Gtk.Threading
import GI.GObject
import qualified Data.Map as M
import Data.Proxy
import qualified GI.Gtk as Gtk
import qualified GI.Gtk.Objects.Container as Container
import qualified GI.WebKit2.Objects.WebView as WebView

import Streamly
import qualified Streamly.Prelude as S
import Neutron.Widget
import Neutron.Diff
import Neutron.Event
import Neutron.WidgetId
import Neutron.WidgetTree
import Neutron.Props.WidgetProp
import Neutron.Props.Internal
import Neutron.Common
import Data.Typeable
import Data.Maybe
import Debug.Trace

-- Turn our list of decls into a real thing
buildWidgetTree :: Typeable s => Gtk.IsContainer c => Maybe c -> [Action s] -> s -> WidgetTree s (SerialT IO) ()
buildWidgetTree container actions s = do
  WidgetTreeState{..} <- ask 
  liftIO . atomically $ writeTVar wtsEventsSupressed True
  forM_ actions $ \case
    UpdateProps wid ctr props -> handleUpdateProps wid ctr props s
    UpdateChildren w ctrType actions -> handleUpdateChildren w ctrType actions s
    DeleteWidget w -> handleDelete container w
    AddWidget w -> handleAdd container w s
    ActionStateEffect (DStateEffect eProxy widget cb) -> handleStateEffect eProxy widget cb s
  liftIO . atomically $ writeTVar wtsEventsSupressed False

-- Update all the children of a container
handleUpdateChildren :: Typeable s => DWidget s -> ContainerType -> [Action s] -> s -> WidgetTree s (SerialT IO) ()
handleUpdateChildren (DWidget Widget{..}) ctrType actions s = do
  WidgetTreeState{..} <- ask 
  widgetMap <- liftIO . readTVarIO $ wtsWidgetMap
  withContainer ctrType $ \p -> do
    inst <- fromWidget p (widgetMap M.! widgetId)
    buildWidgetTree (Just inst) actions s

handleStateEffect :: ( HasEvent w e a
                     , NeutronWidget w
                     , IsObject w
                     ) => Proxy e -> Widget s w -> (a -> StateEffect s ()) -> s -> WidgetTree s (SerialT IO) ()
handleStateEffect eProxy widget cb s = do
  wts@WidgetTreeState{..} <- ask 
  flip evalWidgetTree wts $ do
    widgetMap <- liftIO . readTVarIO $ wtsWidgetMap
    let widgetInst = (widgetMap M.! (widgetId widget))
    widgetInstance <- fromWidget widget widgetInst
    (handlerId, eventStream) <- liftIO $ widgetEvent eProxy widgetInstance
    obj <- toObject widgetInstance
    liftIO . atomically . modifyTVar wtsEventHandlers $ ((obj, handlerId):)
    wd <- lift eventStream
    eventsSupressed <- liftIO . readTVarIO $ wtsEventsSupressed
    if not eventsSupressed then do
      (_, newState) <- lift . flip runStateT s . flip runReaderT widgetMap . unStateEffectT $ cb wd
      liftIO . atomically $ writeTChan wtsState newState
    else pure ()

handleUpdateProps :: WidgetId -> DWidgetTag s -> [WidgetProp Demoted] -> s -> WidgetTree s (SerialT IO) ()
handleUpdateProps wid (DWidgetTag widgetCtr) props s = do
  WidgetTreeState{..} <- ask 
  widgetMap <- liftIO . readTVarIO $ wtsWidgetMap
  widget <- fromWidget widgetCtr (widgetMap M.! wid)
  liftIO . postGUIASync $ mapM_ (setProp widget) props
          
handleDelete :: (Gtk.IsContainer c, MonadIO m) => Maybe c -> DWidget s -> WidgetTree s m ()
handleDelete container (DWidget w@Widget{..}) = deleteWidget container w

handleAdd :: (Gtk.IsContainer c, Typeable s) => Maybe c -> DWidget s -> s -> WidgetTree s (SerialT IO) ()
handleAdd container dw@(DWidget w) s = case widgetConstructor w of
    Window decls -> makeWindow container dw s decls
    Button -> void $ makeWidget @Gtk.Button container dw ()
    LinkButton -> void $ makeWidget @Gtk.LinkButton container dw ()
    Entry -> void $ makeWidget @Gtk.Entry container dw ()
    SearchEntry -> void $ makeWidget @Gtk.SearchEntry container dw ()
    Neutron.Widget.Label -> void $ makeWidget @Gtk.Label container dw ()
    WebView -> void $ makeWidget @WebView.WebView container dw ()
    HBox boxDecls -> void $ addBox @Gtk.HBox container dw () s boxDecls
    VBox boxDecls -> void $ addBox @Gtk.VBox container dw () s boxDecls
    HeaderBar boxDecls -> do
      headerBar <- addBox @Gtk.HeaderBar @Gtk.Window Nothing dw () s boxDecls
      WidgetTreeState{..} <- ask 
      window <- liftIO . readTVarIO $ wtsWindow
      maybe (pure ()) (\w -> liftIO . postGUIASync $ Gtk.windowSetTitlebar w (Just headerBar)) window
    SearchBar boxDecls -> void $ addBox @Gtk.SearchBar container dw () s boxDecls

makeWindow :: (Gtk.IsContainer c, Typeable s) => Maybe c -> DWidget s -> s -> [Decl s]  -> WidgetTree s (SerialT IO) ()
makeWindow container dw@(DWidget Widget{..}) s decls = do
  WidgetTreeState{..} <- ask
  window <- makeWidget @Gtk.Window container dw ()
  liftIO . atomically $ writeTVar wtsWindow (Just window)
  asContainer <- Gtk.toContainer window
  buildWidgetTree (Just asContainer) (diff mempty decls) s
  liftIO . postGUIASync $ #show window
  void $ on window #destroy $ postGUIASync Gtk.mainQuit
    
addBox :: ( Gtk.IsContainer w
          , Gtk.IsContainer c
          , Gtk.IsWidget w
          , Constructible w AttrConstruct
          , Typeable s, NeutronWidget w
          ) => Maybe c -> DWidget s -> ConstructData w -> s -> [Decl s] -> WidgetTree s (SerialT IO ) w
addBox container (DWidget Widget{..}) constructData s boxDecls = do
  WidgetTreeState{..} <- ask 
  box <- construct constructData
  liftIO . postGUIASync $ do
    maybe (pure ()) (flip Gtk.containerAdd box) container
    mapM_ (setProp box . demote) widgetProps
    Gtk.widgetShow box
  asContainer <- Gtk.toContainer box
  asWidget <- Gtk.toWidget box
  liftIO . atomically $ modifyTVar wtsWidgetMap (M.insert widgetId asWidget)
  buildWidgetTree (Just asContainer) (diff mempty boxDecls) s
  pure box

deleteWidget :: (Gtk.IsContainer c, MonadIO m, NeutronWidget w, Gtk.IsWidget w) => Maybe c -> Widget s w -> WidgetTree s m ()
deleteWidget container widget = do
  WidgetTreeState{..} <- ask 
  widgetInst <- liftIO . atomically $ do
    widgetMap <- readTVar wtsWidgetMap
    modifyTVar wtsWidgetMap $ M.delete (widgetId widget)
    pure $ widgetMap M.! (widgetId widget)
  maybe (pure ()) (flip Container.containerRemove widgetInst) container

-- Only way I know to ensure a parallel event occurs after previous ones
evalWidgetTree :: WidgetTree s (SerialT IO) () -> WidgetTreeState s -> WidgetTree s (SerialT IO) ()
evalWidgetTree wdgTree wts = lift . S.yieldM . void . forkIO . runStream . flip runWidgetTree wts $ wdgTree

makeWidget ::  ( Gtk.IsWidget w
               , Gtk.IsContainer c
               , MonadIO m
               , Constructible w AttrConstruct
               , NeutronWidget w
               ) => Maybe c -> DWidget s -> ConstructData w -> WidgetTree s m w
makeWidget container (DWidget Widget{..}) constructData = do
  widget <- construct constructData
  liftIO . postGUIASync $ do
    maybe (pure ()) (flip Gtk.containerAdd widget) container
    mapM_ (setProp widget . demote) widgetProps
    Gtk.widgetShow widget
  WidgetTreeState {..} <- ask
  asWidget <- Gtk.toWidget widget
  liftIO . atomically $ modifyTVar wtsWidgetMap (M.insert widgetId asWidget)
  pure widget