module Main where

import Neutron
import Data.Semigroup
import qualified Data.Text as T
import Data.Functor
import Control.Monad.State.Class as State
import Data.Typeable

main :: IO ()
main = neutron (Just "example/main.css") noTodos $ \ToDoState{..} -> do
  window [#title :~ "ToDo"] $ do
    headerBar [#title :~ "todo", #showCloseButton := True] $ do
      clearButton <- widget Button [#label :~ "Clear", #name :~ "clear"]
      onEvent clearButton clicked . const . put $ noTodos
    vBox mempty $ do
      hBox mempty $ do
        entry <- widget Entry [#placeholderText :~ "Add an item", #text := currentItem]
        addButton <- widget Button [#label :~ "Add"]
        onEvent entry textChanged $ \t -> do
          modify $ \s -> s { currentItem = t}
        onEvent addButton clicked . const . modify
          $ \(ToDoState current items) -> ToDoState { currentItem = "", items = current : items}
      vBox [] $ 
        mapM_ (\t -> widget Label [#label := t]) items

data ToDoState = ToDoState { currentItem :: T.Text, items :: [T.Text] }
  deriving (Show, Typeable)

noTodos :: ToDoState
noTodos = ToDoState mempty mempty